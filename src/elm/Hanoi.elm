module Hanoi exposing (..)

import List.Extra


---- MODEL Helpers


type Pole
    = Left
    | Middle
    | Right


type alias DiskId =
    Int


type alias DiskPosition =
    Int


type alias Disk =
    { pole : Pole, id : DiskId, position : DiskPosition }


type alias Disks =
    List Disk


init : Disks
init =
    List.range 0 6 |> List.map (\i -> Disk Left i (6 - i))


poles : List Pole
poles =
    [ Left, Middle, Right ]


poleDisks : Disks -> Pole -> Disks
poleDisks allDisks pole =
    List.filter (\disk -> disk.pole == pole) allDisks


sortedPoleDisks : Disks -> Pole -> Disks
sortedPoleDisks allDisks pole =
    poleDisks allDisks pole |> List.sortBy .position |> List.reverse


topOfPole : Disks -> Pole -> Maybe Disk
topOfPole allDisks pole =
    List.head (sortedPoleDisks allDisks pole)


{-| XXX have to be already sorted
-}
canDrag : Disks -> Disk -> Bool
canDrag poleDisks disk =
    let
        maybeTopDisk =
            List.head poleDisks
    in
        case maybeTopDisk of
            Nothing ->
                False

            Just topDisk ->
                disk == topDisk


canDrop : Maybe Disk -> Disk -> Bool
canDrop maybeTargetDisk disk =
    case maybeTargetDisk of
        Nothing ->
            True

        Just targetDisk ->
            disk.id < targetDisk.id


drop : Disks -> Disk -> Pole -> Disks
drop allDisks disk pole =
    let
        ( targetPoleDisks, restDisks ) =
            List.partition (\d -> d.pole == pole) allDisks

        maybeTargetTopDisk =
            List.head targetPoleDisks

        -- update disk position
        canUpdate d =
            d.id == disk.id && canDrop maybeTargetTopDisk disk

        update d =
            case Debug.log "afd" maybeTargetTopDisk of
                Nothing ->
                    { d | pole = pole, position = 0 }

                Just targetTopDisk ->
                    { d | pole = pole, position = targetTopDisk.position + 1 }

        updatedRestDisks =
            List.Extra.updateIf canUpdate update restDisks

        -- update target pole disks position
        updatedTargetPoleDisks =
            targetPoleDisks
    in
        updatedTargetPoleDisks ++ updatedRestDisks
