module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html5.DragDrop as DragDrop
import Hanoi


main : Program Never Model Msg
main =
    beginnerProgram
        { model = model
        , update = update
        , view = view
        }



-- MODEL


type alias Model =
    { disks : Hanoi.Disks
    , ddmodel : DragDrop.Model Hanoi.Disk Hanoi.Pole
    }


model : Model
model =
    { disks = Hanoi.init
    , ddmodel = DragDrop.init
    }



-- UPDATE


type Msg
    = DragDropMsg (DragDrop.Msg Hanoi.Disk Hanoi.Pole)


update : Msg -> Model -> Model
update msg model =
    case msg of
        DragDropMsg ddmsg ->
            let
                ( ddmodel, ddresult ) =
                    -- Debug.log "" (DragDrop.update ddmsg model.ddmodel)
                    DragDrop.update ddmsg model.ddmodel
            in
                { model
                    | ddmodel = ddmodel
                    , disks =
                        ddresult
                            |> (Maybe.map <| uncurry <| Hanoi.drop model.disks)
                            |> Maybe.withDefault model.disks
                }



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "main" ] <| List.map (viewPole model) Hanoi.poles


viewPole : Model -> Hanoi.Pole -> Html Msg
viewPole model pole =
    let
        disks =
            Hanoi.sortedPoleDisks model.disks pole

        attributes =
            [ class "pole" ] ++ DragDrop.droppable DragDropMsg pole
    in
        div
            [ class "column" ]
            [ div attributes <| List.map (viewDisk disks) disks
            ]


viewDisk : Hanoi.Disks -> Hanoi.Disk -> Html Msg
viewDisk poleDisks disk =
    let
        width =
            20 * disk.id + 40

        styles =
            [ ( "width", toString width ++ "px" ) ]

        draggableAttrigutes =
            if Hanoi.canDrag poleDisks disk then
                DragDrop.draggable DragDropMsg disk
            else
                []

        attributes =
            [ class "disk", style styles ] ++ draggableAttrigutes
    in
        div
            attributes
            [ text <| toString disk.id ]
